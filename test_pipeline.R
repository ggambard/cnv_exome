
# Start Configuration
# ---------------------------------------------------------------------------------------------------------------------------------

# Library #
# source("/home/gambardg/repo/CNV_exome/cnv_functions.R")
# require(preprocessCore)

# Sample Config. #
# sample="S63800" # Sample Name
# spec = "mouse" # Specie (i.e. human or mouse)

# File Config. #
# workDir="/home/gambardg/outCnv/"
# tumorBam="/home/gambardg/cnv_pipeline/S63800/Sample_Ly63800.bam" # tumor bam file
# normalBam="/home/gambardg/cnv_pipeline/S63800/Sample_N63800.bam" # normal bam file
# tumorVcf = "/home/gambardg/cnv_pipeline/S63800/Sample_Ly63800.vcf" # Var call file of the tumor (must be in vcf format)
# normalVcf = "/home/gambardg/cnv_pipeline/S63800/Sample_N63800.vcf" # Var call file of the normal (must be in vcf format)

# bedtoolsDir = "/home/gambardg/bin_tools/bedtools_2.19.1/" # Folder in which bedtool is installed
# targetFile="/home/gambardg/cnv_pipeline/SureSelect_Mouse_All_Exon_V1_Annotated_G7500_G7550_20110119_bed" # Agilent Target bed file

# Library #
source("/home/sinhas/ssinha/repo/cnv_exome/cnv_functions.R")
require(preprocessCore)
require(ggplot2)
require(tablegrob)
require(gridExtra)


# Sample Config. #
sample="tAML-05" # Sample Name
spec = "human" # Specie (i.e. human or mouse)

# File Config. #
workDir="/home/sinhas/ssinha/repo/outCnv/"
tumorBam="/home/sinhas/ssinha/Modhep_method/NewDatasets/MDS/BAM/DRS000818/DRX000883/DRX000883_srt_OT.bam" # tumor bam file
normalBam="/home/sinhas/ssinha/Modhep_method/NewDatasets/MDS/BAM/DRS000818/DRX000884/DRX000884_srt_OT.bam" # normal bam file
varscanSNP ="/home/sinhas/ssinha/Modhep_method/NewDatasets/MDS/variants/tAML-05.var.snp"

bedtoolsDir = "/home/sinhas/ssinha/sw/bedtools-2.19.1/bin" # Folder in which bedtool is installed
targetFile="/home/sinhas/ssinha/human/AgilentTargets/SureSelect38Mb/S0274956/S0274956_Covered.bed" # Agilent Target bed file

cytoPath="/home/sinhas/ssinha/repo/cnv_exome/cytobands/hg19.cytoBand.txt"

# End Configuration
# -----------------------------------------------------------------------------------------------------------------------------------

####################################
#  0. Compute the necessary files  #
####################################

mkdir(workDir) # make the working dir
mkdir(workDir,sample) # make the output dir 

outDir = paste(workDir,"/",sample,coll="",sep="") # Out Directory
BAFfile = paste(outDir,"/",sample,".germline.bed",coll="",sep="") # File with BAF values
tumorHistPath = paste(outDir,"/",sample,".tumor.hist.txt",coll="",sep="") # Hist coverage file that will be generated with samtool
normalHistPath = paste(outDir,"/",sample,".normal.hist.txt",coll="",sep="") # Hist coverage file that will be generated with samtool

# 0.1 BAF file
# -------------
# freqdata = get_germline(tumorVcf,normalVcf,BAFfile) # function to create germline object from mutation calls on tumor and normal 

freqdata = get_germline_from_varscanSomatic(varscanSNP,BAFfile) # function to create germline object from somatic calls 

# 0.2 Make hist coverage files for tumor and normal
# --------------------------------------------------
get_hist_coverage(tumorBam,normalBam,targetFile,sample,bedtoolsDir,outDir) 

# 0.3 Compute the coverage for each target
# ----------------------------------------------------
# covT = get_target_coverage(tumorHistPath,outDir,sample,"T", ncores = 16)
# covN = get_target_coverage(normalHistPath,outDir,sample,"N", ncores = 16)

covT = get_target_coverage(tumorHistPath,outDir,sample,"T", ncores = 16)
covN = get_target_coverage(normalHistPath,outDir,sample,"N", ncores = 16)

# 0.4 Split the targets in genes
# --------------------------------------
geneCov = get_gene_coverage(covT, covN, outDir, sample, ncores = 16, sep = ",")


#############################################
# 1. Call Copy Number Variation from Exome  #
#############################################
setwd(outDir)

# 1.1 Get graphics data for plotting 
# -----------------------------------
genomeD=get_genome_info(species=spec)
matrixGraphs=plotGraphics(genomeD)


# 1.2 Finally call the estimation of CNV
# --------------------------------------
mainMethod(geneCov,sample,freqdata,matrixGraphs,outDir,cytoPath,species=spec)


# 1.3 Multiple sample analysis
# --------------------------------------
multiple_sample_analysis(filelist,outDir,matrixGraphs,cytoPath)


